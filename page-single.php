<?php
/**
* Template Name: Single page Legal
*/

// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

get_header();?>

<main class="w-100 mx-auto">
	<div class="container-fluid pb-4">
      <div class="d-flex flex-row">
		  <h1 class="modal-heading"><?php echo get_the_title(); ?></h1>
		  	<a href="/" class="a-close"><svg xmlns="http://www.w3.org/2000/svg" height="25" viewBox="0 -960 960 960" width="25"><path d="m256-236-20-20 224-224-224-224 20-20 224 224 224-224 20 20-224 224 224 224-20 20-224-224-224 224Z"/></svg></a>

      </div>
</div>
	
<section class="container-fluid">
	<div class="row">
    <div class="col-md-4 ">
							
<a  href="https://temporal.upcofly.com/aviso-legal/" id="avisoLegal" class="animationButton">
<p class="m-0">
							Aviso legal <br> <small class="text-muted">Información legal</small>
</p>
					<svg xmlns="http://www.w3.org/2000/svg" height="25" viewBox="0 -960 960 960" width="25"><path d="m232-212-20-20 448-448H360v-28h348v348h-28v-300L232-212Z"/></svg>
					</a>
<a href="https://temporal.upcofly.com/politica-de-privacidad/" id="politicaPrivacidad" class="animationButton ">
						<p class="m-0">
							Política de privacidad<br> <small class="text-muted">Información legal</small>
						</p>
					<svg xmlns="http://www.w3.org/2000/svg" height="25" viewBox="0 -960 960 960" width="25"><path d="m232-212-20-20 448-448H360v-28h348v348h-28v-300L232-212Z"/></svg>
					</a>
<a id="cookies" class="animationButton">
						<p class="m-0">
							Política de cookies <br> <small class="text-muted">Información legal</small>
						</p>
					<svg xmlns="http://www.w3.org/2000/svg" height="25" viewBox="0 -960 960 960" width="25"><path d="m232-212-20-20 448-448H360v-28h348v348h-28v-300L232-212Z"/></svg>
					</a>
</div>
    <div class="col-md-7 mx-auto pt-md-0 pt-4">
		<?php echo get_the_content(); ?>

</div>

  </div>
	
	
</section>
</main>











<?php get_footer();