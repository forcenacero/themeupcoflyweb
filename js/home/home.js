document.querySelectorAll(".slider").forEach((item) => {
  let clone = item.querySelector(".slider-items").cloneNode(true);
  clone.classList.add("clone");
  clone.ariaHidden = true;
  item.append(clone);
});


// up welcome

var quotes = document.querySelectorAll(".quotes");
var quoteIndex = -1;

function showNextQuote() {
  ++quoteIndex;
  var currentQuote = quotes[quoteIndex % quotes.length];
  fadeInAndOut(currentQuote, 2000, 2000, 2000, showNextQuote);
}

showNextQuote();

// up-one

var quotesOne = document.querySelectorAll(".quotesOne");
var quoteIndexOne = -1;

function showNextQuoteOne() {
  ++quoteIndexOne;
  var currentQuoteOne = quotesOne[quoteIndexOne % quotesOne.length];
  fadeInAndOut(currentQuoteOne, 1000, 1000, 1000, showNextQuoteOne);
}

showNextQuoteOne();

// end up-one

function fadeInAndOut(element, fadeInDuration, delayDuration, fadeOutDuration, callback) {
  element.style.display = "block";
  element.style.opacity = 0;

  var fadeInInterval = setInterval(function () {
    var currentOpacity = parseFloat(element.style.opacity);
    if (currentOpacity < 1) {
      element.style.opacity = currentOpacity + 0.01;
    } else {
      clearInterval(fadeInInterval);
      setTimeout(function () {
        var fadeOutInterval = setInterval(function () {
          var currentOpacity = parseFloat(element.style.opacity);
          if (currentOpacity > 0) {
            element.style.opacity = currentOpacity - 0.01;
          } else {
            clearInterval(fadeOutInterval);
            element.style.display = "none";
            if (callback) {
              callback();
            }
          }
        }, fadeOutDuration / 100);
      }, delayDuration);
    }
  }, fadeInDuration / 100);
}



