<?php
/**
* Template Name: Home
*/

// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

get_header();?>

<main class="scroll-container">
    <section class="section up-first scroll-item">
		<div class="container-fluid h-100">
		<div class="row">
			<div class="upLeft col-md-7 d-flex justify-content-center align-items-start flex-column p-5">
				<h1 class="homeTitle">First freelance team</h1>
        		<h2 >Especialized in digital products, comunication and events.</h2>
			</div>
			
			<div class="upRight col-md-5">
				<div class="grid">
					<div class="slider" style="--delay:-10s;--slow:-5s">
						<div class="slider-items">
						  <div class="item">
							<a href="https://tomjacobs.co.uk/" target="_blank"></a>
							<p style="width:100%; height:200px; border-radius:20px; border:1px solid black; padding:20px;">Your project</p>
						  </div>
						  <div class="item">
							<a href="https://tomjacobs.co.uk/" target="_blank"></a>
							<img src="https://source.unsplash.com/random/300x450/?earth,2" alt="" width="300" height="450">
						  </div>
						  <div class="item">
							<a href="https://tomjacobs.co.uk/" target="_blank"></a>
							<img src="https://source.unsplash.com/random/300x450/?earth,3" alt="" width="300" height="450">
						  </div>
						  <div class="item">
							<a href="https://tomjacobs.co.uk/" target="_blank"></a>
							<img src="https://source.unsplash.com/random/300x450/?earth,4" alt="" width="300" height="450">
						  </div>
						</div>
					</div>

				  <div class="slider" style="--delay:-10s;--slow:2s">
					<div class="slider-items">
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?fire,1" alt="" width="300" height="450">
					  </div>
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?fire,2" alt="" width="300" height="450">
					  </div>
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?fire,3" alt="" width="300" height="450">
					  </div>
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?fire,4" alt="" width="300" height="450">
					  </div>
					</div>
				  </div>
				  <div class="slider hidden-m" style="--delay:-20s;--slow:-2s">
					<div class="slider-items">
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?water,1" alt="" width="300" height="450">
					  </div>
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?water,2" alt="" width="300" height="450">
					  </div>
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?water,3" alt="" width="300" height="450">
					  </div>
					  <div class="item">
						<a href="https://tomjacobs.co.uk/" target="_blank"></a>
						<img src="https://source.unsplash.com/random/300x450/?water,4" alt="" width="300" height="450">
					  </div>
					</div>
				  </div>
				</div>
			</div>
			
		</div>
	</div>
       
		
    </section>
	
    <section class="section up-second scroll-item" >
        <div class="up-one-animation">
            <span>One</span>
            <div class="message">
                <div class="word1">contact</div>
                <div class="word2">budget</div>
                <div class="word3">team</div>
            </div>
        </div>
        <p class="homeSubtitle">To offer a complete advice adapted to customer needs, covering all areas.</p>
    </section>

	<section id="agility" class="section up-third scroll-item clase-elemento">
		<video autoplay muted loop id="agilityVideo">
  			<source src="http://temporal.upcofly.com/wp-content/themes/upcofly/assets/agilityVideo.mp4" type="video/mp4">
		</video>
		<div class="overlay"></div>
		<div class="container">
			 <h2 class="homeTitle text-white text-center">Agility</h2>
		<p class="homeSubtitle text-white">Custom-made freelance team tailored to your needs. <br> Working collaboratively, proactively, and efficiently in project mode.</p>		
		</div>
    </section>
	
	<section class="up-section-columns scroll-item h-50">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2 class="homeTitle text-dark text-left font-weight-bold">
						Infinite freelancers with <br>infinite skills.
					</h2>
				</div>
				<div class="col-md-6">
					<p class="homeSubtitle">
						As of today, our strength lies in being able to offer, through a single point of contact and a unified budget, tailored comprehensive guidance to each client, covering all related areas.
					</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="section up-four scroll-item" >
		<div class="overlay"></div>
		<div class="container">
			 <h2 class="homeTitle text-white text-center">Authenticity</h2>
		<p class="homeSubtitle text-white">Complementary profiles working collectively
to achieve a common goal.</p>		
		</div>
    </section>
	
	<section class="up-section-columns scroll-item h-50" style="background:#f5f5f7;">
		<div class="container">
			<h2 class="homeTitle text-dark text-left font-weight-bold">Servicios de Upcofly.</h2>
			<div class="row pt-3">
				<div class="col-md-3 d-flex flex-column">
					<a>Branding</a>
					<a>Branding</a>
					<a>Branding</a>
				</div>
				<div class="col-md-3 d-flex flex-column">
					<a>Branding</a>
					<a>Branding</a>
					<a>Branding</a>
				</div>
				<div class="col-md-3 d-flex flex-column">
					<a>Branding</a>
					<a>Branding</a>
					<a>Branding</a>
				</div>
				<div class="col-md-3 d-flex flex-column">
					<a>Branding</a>
					<a>Branding</a>
					<a>Branding</a>
				</div>
				
			</div>
		</div>
	</section>
	
</main>








<?php get_footer();