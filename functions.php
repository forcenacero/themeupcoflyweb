<?php
/**
 * upcofly Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package upcofly
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_UPCOFLY_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'upcofly-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_UPCOFLY_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

//REGISTRAR ESTILOS GLOBALES
function upcofly_enqueue_styles() {

    //bootstrap
    wp_enqueue_style ('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css');
	wp_enqueue_style( 'load-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css' );
 
}
add_action( 'wp_enqueue_scripts', 'upcofly_enqueue_styles');

add_action( 'wp', 'remove_astra_header_callback');
function remove_astra_header_callback(){
remove_action( 'astra_header', 'astra_header_markup' );
}

//END REGISTRAR ESTILOS GLOBALES

// REGISTRAR SCRIPTS GLOBALES
function upcofly_enqueue_scripts() {
	
    //bootstrap scripts
    wp_enqueue_script( 'bootstrap-js',  'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js', array( 'jquery' ), '', true );
	
	//jquery
	    wp_enqueue_script( 'jquery-js',  'https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js', array( 'jquery' ), '', true );
	
    
     //js
     wp_enqueue_script( 'upcofly-js', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ), CHILD_THEME_UPCOFLY_VERSION, 'all' );
	 
  }
  add_action( 'wp_enqueue_scripts', 'upcofly_enqueue_scripts');
//END REGISTRAR SCRIPTS GLOBALES

//SVG ACEPT
function custom_mimes( $mimes = array() ) {
	
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'custom_mimes' );
//END SVG ACEPT


/*HOME*/

add_action('wp_head', 'script_home');
function script_home(){
if(is_front_page()) {  
	
	 //js
     wp_enqueue_script( 'home-js', get_stylesheet_directory_uri() . '/js/home/home.js', array( 'jquery' ), CHILD_THEME_UPCOFLY_VERSION, 'all' );
	
	//css
	     wp_enqueue_style( 'home-css', get_stylesheet_directory_uri() . '/css/home.css', CHILD_THEME_UPCOFLY_VERSION, 'all' );
	
	//swiper css
    	//wp_enqueue_style( 'swiper-css',  'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css');
	//swiper js
	    //wp_enqueue_script( 'swiper-js',  'https://cdnjs.cloudflare.com/ajax/libs/Swiper/8.4.5/swiper-bundle.min.js', array( 'jquery' ), '', true );
	
		
	
}
};

/*END HOME*/

/*LANDING*/

add_action('wp_head', 'script_landing');
function script_landing(){
if(is_page_template('page-landing.php')) {  
	
	 //js
     wp_enqueue_script( 'landing-js', get_stylesheet_directory_uri() . '/js/landing/landing.js', array( 'jquery' ), CHILD_THEME_UPCOFLY_VERSION, 'all' );
	
	//css
	     wp_enqueue_style( 'landing-css', get_stylesheet_directory_uri() . '/css/landing.css', CHILD_THEME_UPCOFLY_VERSION, 'all' );
	
	
	
	
}
};

/*END LANDING*/