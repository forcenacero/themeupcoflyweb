<?php
/**
* Template Name: Landing
*/

// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

get_header();?>

<section class="landing">
  <div class="showcase">
    <div class="container">
      <div class="d-flex mb-2">
        <div class="title">
          <h3>Five colors.<br>
            Anything but <br>
            monotone.
          </h3>
        </div>
        <div class="carousel-indicator">
          <div class="carousel-indicator__button silver"></div>
          <div class="carousel-indicator__button grey"></div>
          <div class="carousel-indicator__button blue"></div>
          <div class="carousel-indicator__button grey"></div>
          <div class="carousel-indicator__button green"></div>
        </div>
      </div>
    </div>
    <div class="showcase-container">

      <div class="showcase-left">
        <div class="content">
          <div class="carousel height-100">
            <iv class="carousel-wrapper">
              <div class="carousel-item">
                <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_silver_front__g8c839jaldqy_medium.jpg" class="height-100 width-auto object-fit-contain" alt="">
              </div>
              <div class="carousel-item">
                <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_pink_front__dbqafvrvcy6a_medium.jpg" class="height-100 width-auto object-fit-contain" alt="">
              </div>
              <div class="carousel-item">
                <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_gray_front__bgkzj4cnbafm_medium.jpg" class="height-100 width-auto object-fit-contain" alt="">
              </div>
              <div class="carousel-item">
                <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_blue_front__ddfias5frxqq_medium.jpg" class="height-100 width-auto object-fit-contain" alt="">
              </div>
              <div class="carousel-item">
                <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_green_front__cqpeugza9as2_medium.jpg" class="height-100 width-auto object-fit-contain" alt="">
              </div>
          </div>
        </div>
      </div>

      <div class="showcase-right">
        <iv class="content">
          <div class="preview-image">
            <div class="preview-image__items">
              <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_silver_side__ff3nerq51ka6_medium.jpg" alt="">
            </div>
            <div class="preview-image__items">
              <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_pink_side__gd261969pcmu_medium.jpg">
            </div>
            <div class="preview-image__items">
              <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_gray_side__gbjen9i2hram_medium.jpg">
            </div>
            <div class="preview-image__items">
              <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_blue_side__e166rgbg1s02_medium.jpg">
            </div>
            <div class="preview-image__items">
              <img src="https://www.apple.com/v/airpods-max/e/images/overview/design_colors_green_side__dpusuw9jg30i_medium.jpg">
            </div>
          </div>
      </div>
    </div>

  </div>
</section>










<?php get_footer();