document.addEventListener("DOMContentLoaded", function () {
	
	//Detectar URL
  // Array de objetos con información de URL y el identificador del elemento activo
  var urlElementoActivo = [
    { url: "https://temporal.upcofly.com/aviso-legal/", elementoActivo: "#avisoLegal" },
    { url: "https://temporal.upcofly.com/politica-de-privacidad/", elementoActivo: "#politicaPrivacidad" },
    // Agrega más objetos según sea necesario
  ];

  // Obtiene la URL actual
  var currentURL = window.location.href;

  // Itera a través del array y ajusta las clases según la URL
  urlElementoActivo.forEach(function (obj) {
    if (currentURL.indexOf(obj.url) !== -1) {
      // Agrega la clase activo al elemento específico
      document.querySelector(obj.elementoActivo).classList.add("bg-dark");
    } else {
      // Remueve la clase activo de otros elementos
      document.querySelector(obj.elementoActivo).classList.remove("bg-dark");
    }
  });
  //End detectar URL
	
//////Logo effect
var i = 0;

function mask(a) {
  i++;
  if (i < a) {
    $("#logo").attr("style", "-webkit-mask:-webkit-gradient(radial, 17 17, " + i + ", 17 17, " + (i + 15) + ", from(rgb(0, 0, 0)), color-stop(0.5, rgba(0, 0, 0, 0.2)), to(rgb(0, 0, 0)));");

    setTimeout(function() {
      mask(a);
    }, 20 - i);
  } else i = 0;
}

$("#logo").on("mouseenter", function() {
  mask(138)
});
//End logo effect

////Cambiar color body mes

window.addEventListener("load", cambiarMes);

//Función que transforma un número al nombre del mes para aplicar estilo CSS en función de la fecha actual
function numAMes(numero){
	switch(parseInt(numero)){
		case 1: return "enero";
		case 2: return "febrero";
		case 3: return "marzo";
		case 4: return "abril";
		case 5: return "mayo";
		case 6: return "junio";
		case 7: return "julio";
		case 8: return "agosto";
		case 9: return "septiembre";
		case 10: return "octubre";
		case 11: return "noviembre";
		case 12: return "diciembre";
	}
}

function cambiarMes(){
	let fechaHoy = new Date();
	document.getElementsByTagName("body")[0].classList.add(numAMes(fechaHoy.getMonth()+1));
	
}
//End cambiar color body mes

});

////jQuery
 $(document).ready(function () {
	 //Ocultar o mostrar scroll nav bottom fixed
    var lastScrollTop = 0;
    var delta = 10; // Puedes ajustar este valor según sea necesario

    $(window).scroll(function () {
      var st = $(this).scrollTop();

      // Verificar si el desplazamiento es más de 10px en cualquier dirección
      if (Math.abs(lastScrollTop - st) <= delta)
        return;

      if (st > lastScrollTop && st > delta) {
        // Scroll hacia abajo
        $('#up-footer-nav-float').css('bottom', '-100px'); // Ocultar el header
		  $('.navItems, .up-navbar').removeClass('open');
		  $('.iconPlus').removeClass('rotate45');
      } else {
        // Scroll hacia arriba o scroll en la parte superior
        if (st + $(window).height() < $(document).height()) {
          $('#up-footer-nav-float').css('bottom', '30px'); // Mostrar el header
        }
      }

      lastScrollTop = st;
    });
	 //End ocultar o mostrar scroll nav bottom fixed
	 
	 //Modal toggle
		$('.modal-toggle').on('click', function(e) {
  		e.preventDefault();
  		$('.modal-contact').toggleClass('is-visible');
		if ($('.newFreelance, .newProject, .modal-header').hasClass('d-none')) {
			$('.newFreelance, .newProject, .modal-header').removeClass('d-none');
		} 
		else if ($('.contactProject').hasClass('d-flex')) {
			$('.contactProject').removeClass('d-flex');
		}
		else if ($('.contactFreelance').hasClass('d-flex')) {
			$('.contactFreelance').removeClass('d-flex');
		}
		else { 
		$('.newFreelance, .newProject, .modal-header').addClass('d-flex');
		}

		});
	//End modal toggle
	
	//Nav responsive toggle
	$('.brand').on('click', function(e) {
  		e.preventDefault();
  		$('.navItems, .up-navbar').toggleClass('open');
		$('.iconPlus').toggleClass('rotate45');
		});

	 
	  // Elementos a los que les quieres remover la clase
      var elementos = $('.navItems, .up-navbar, .iconPlus');

      // Detectar clic fuera de los elementos
      $(document).on("click", function(e) {
        if (!elementos.is(e.target) && elementos.has(e.target).length === 0) {
          // Clic fuera de los elementos, remueve la clase
          elementos.removeClass("open");
			elementos.removeClass("rotate45");
        }
      });
	//End nav responsive toggle
	
	 //Forms modal
	 $('.newProject').on('click', function(e) {
  		e.preventDefault();
  		$('.contactProject').toggleClass('d-flex');
		 $('.newFreelance, .newProject, .modal-header').toggleClass('d-none');
		});
	  $('.newFreelance').on('click', function(e) {
  		e.preventDefault();
  		$('.contactFreelance').toggleClass('d-flex');
		 $('.newFreelance, .newProject, .modal-header').toggleClass('d-none');
		});
	 //End forms modal
	 
  });

//jQuery







