<?php
/**
* Template Name: Home V2
*/

// Exit if accessed directly
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

get_header();?>

<main class="scroll-container">
    <section class="section up-first scroll-item">
		<div class="container-fluid h-100">
			<div class="up-welcome">
				<div>
					<p>Sabemos que 
						 <span class="quotes">tienes poco tiempo.</span>
						 <span class="quotes">quieres cuidar tu marca.</span>
						 <span class="quotes">tienes muchas tareas.</span>
						 <span class="quotes">quieres crear contenido.</span>
						 <span class="quotes">te gusta lo diferente.</span>
						 <span class="quotes">quieres estar actualizado.</span>
						 <span class="quotes">te gustaría tener una estrategia de marketing.</span>
						 <span class="quotes">quieres diferenciarte de tu competencia.</span>
						 <span class="quotes">no llegas a todo.</span>
						<span class="quotes">quieres todo en uno.</span>
						<span class="quotes">te gustaría volar.</span>
					</p>
				</div>
				
				
			</div>

		</div>
       
		
    </section>

	<section class="section up-second scroll-item">
		<div class="container">
			<div class="row">
				<div>

					
					<h1 class="homeTitle">Somos el primer equipo de freelances</h1>
					<h2 class="homeSubtitle">Especializados en productos digitales, comunicación y eventos.</h2>
					
				</div>
			</div>
		</div>
       
		
    </section>
	
    <section class="section up-third scroll-item" >
        <div class="up-one-animation">
            <p>Un<br>
				<span class="quotesOne">contacto</span>
				<span class="quotesOne">presupuesto</span>
				<span class="quotesOne">equipo</span>
			</p>
        </div>
		<p class="homeSubtitle text-center">Para ofrecer un asesoramiento completo a medida de cada cliente, <br>abarcando todas las áreas vinculadas.</p>
    </section>
	


	<section class="up-section-columns">
		
			<div class="row">
				<div class="col-md-4">
					<h2 class="homeTitle text-left font-weight-bold">Digital Product & Branding</h2>
					<h2 class="homeSubtitle">Construimos marcas, productos y servicios para el mañana.</h2>
				</div>
				<div class="col-md-8">
					
					<div class="accordion accordion-flush" id="accordionFlushServices">
  <div class="accordion-item mt-0">
    <h2 class="accordion-header mt-0" id="flush-headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
        Diseño de producto
      </button>
    </h2>
    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushServices">
      <div class="accordion-body">Transformamos ideas en experiencias, diseñamos productos y servicios digitales sólidos a través de estrategia, diseño, datos y tecnología. Convierte tu sueño en realidad con nuestro equipo de expertos en diseño de producto.</div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
	  Branding estratégico
      </button>
    </h2>
    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushServices">
      <div class="accordion-body">En un mundo lleno de mensajes y ofertas que nos rodean, el diseño de marca se posiciona como un distintivo esencial para destacar y conectar de manera única con la audiencia deseada.</div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
        Sistemas de diseño
      </button>
    </h2>
    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushServices">
      <div class="accordion-body">Al comprender las necesidades, establecemos cimientos sólidos para su negocio, tomando decisiones estratégicas que evolucionan junto con su marca y su entorno. Desarrollamos un lenguaje coherente y preciso, donde las decisiones sobre estilo, funcionalidad, componentes, organización y metodología se estudian minuciosamente para perdurar en el tiempo. Creamos uniformidad y optimizamos sus productos utilizando las herramientas necesarias de manera efectiva.</div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-headingFour">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
        Diseño de operaciones
      </button>
    </h2>
    <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushServices">
      <div class="accordion-body">Analizamos y supervisamos las operaciones diarias de tu empresa, desde los procesos hasta las herramientas. Tu empresa se construye sobre las personas de tu equipo, por lo que diseñamos metodologías y herramientas adaptadas a la realidad de tu negocio para impulsar tu crecimiento continuo. Mantenemos una constante actualización, incorporando mejores prácticas trabajando de forma compartida fomentando un sentido de comunidad. Innovamos y desarrollamos herramientas personalizadas para automatizar procesos, impulsando la eficiencia operativa.</div>
    </div>
  </div>
</div>
				</div>
			</div>
		
	</section>

	<section class="up-section-columns scroll-item">
		
			<div class="row">
				<div class="col-md-8 bg-dark p-5 borderRadius">
					<h2 class="homeTitle text-white">Freelances infinitos con infinitas habilidades.</h2>
					<p class="homeSubtitle text-white">A día de hoy nuestra fortaleza es poder ofrecer mediante un solo contacto y presupuesto único, un asesoramiento completo a medida de cada cliente y momento, abarcando todas las áreas vinculadas.</p>
				</div>
				
			</div>
	
    </section>
	
	<section class="up-section-columns">
		
			<div class="row">
				<div class="col-md-4">
					<h2 class="homeTitle text-dark text-left font-weight-bold">
						Valores
					</h2>
					<p class="homeSubtitle mb-4">
					Buscamos un impacto significativo sostenible.
					</p>
				</div>
				<div class="col-md-8">
					<div class="flex-md-row d-flex flex-column justify-content-md-between align-items-center">
				<article class="box bg-light m-1">
					<h2 class="homeTitle text-dark text-left font-weight-bold">
						Agilidad
					</h2>
					<p class="text-muted">
						Equipo de freelances creado a medida en función de las necesidades. Trabajando en modo proyecto de forma colaborativa, proactiva y eficaz.
					</p>
				</article>
				<article class="box bg-light m-1">
					<h2 class="homeTitle text-dark text-left font-weight-bold">
						Autenticidad
					</h2>
					<p class="text-muted">
						Perfiles complementarios trabajando colectivamente para alcanzar un mismo objetivo.
					</p>
				</article>
		 <article class="box bg-light m-1">
					<h2 class="homeTitle text-dark text-left font-weight-bold">
						Flexibilidad
					</h2>
					<p class="text-muted">
						Creación de diferentes equipos con talento que satisfacen necesidades específicas.
					</p>
				</article>
			</div>
				</div>
			</div>
		
	</section>

	
 <section class="section up-four scroll-item" >
     <div class="flex-md-row d-flex flex-column justify-content-md-between align-items-center">
				<article class="box bg-dark m-2">
					<h2 class="homeTitle text-white text-left font-weight-bold">
						Agilidad
					</h2>
					<p class="text-white">
						Equipo de freelances creado a medida en función de las necesidades. Trabajando en modo proyecto de forma colaborativa, proactiva y eficaz.
					</p>
				</article>
				<article class="box bg-dark m-2">
					<h2 class="homeTitle text-white text-left font-weight-bold">
						Autenticidad
					</h2>
					<p class="text-white">
						Perfiles complementarios trabajando colectivamente para alcanzar un mismo objetivo.
					</p>
				</article>
		 <article class="box bg-dark m-2">
					<h2 class="homeTitle text-white text-left font-weight-bold">
						Flexibilidad
					</h2>
					<p class="text-white">
						Creación de diferentes equipos con talento que satisfacen necesidades específicas.
					</p>
				</article>
			</div>
    </section> 

	<section class="up-section-columns">
	<p class="homeSubtitle ">Sabemos que necesitas ayuda.</p>
	<h2 class="homeTitle pb-3">¿Cómo podemos ayudarte?</h2>

		<div class="container">
		<div class="row">
			<div class="col-md-6">
			<a href="https://temporal.upcofly.com/politica-de-privacidad/" id="politicaPrivacidad" class="animationButton">
						<p class="m-0">
						Me gustaría trabajar con vosotros <br> <small class="text-muted">Tengo un proyecto</small>
						</p>
					<svg xmlns="http://www.w3.org/2000/svg" height="25" viewBox="0 -960 960 960" width="25"><path d="m232-212-20-20 448-448H360v-28h348v348h-28v-300L232-212Z"></path></svg>
					</a>
			</div>
			<div class="col-md-6">
			<a href="https://temporal.upcofly.com/politica-de-privacidad/" id="politicaPrivacidad" class="animationButton">
						<p class="m-0">
						Quiero unirme al equipo <br> <small class="text-muted">Soy freelance</small>
						</p>
					<svg xmlns="http://www.w3.org/2000/svg" height="25" viewBox="0 -960 960 960" width="25"><path d="m232-212-20-20 448-448H360v-28h348v348h-28v-300L232-212Z"></path></svg>
					</a>
			</div>
			
		</div>

		</div>
		

</section>
<div class="container">
    <div class="row">
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 10,
            'category_name' => 'project',
        );

        $query = new WP_Query($args);

        if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();
                ?>
                <div class="col-md-4">
                    <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post'); ?>>
					<a href="<?php the_permalink(); ?>">
                        <div class="card borderRadius text-white overflow-hidden">
                            <?php if (has_post_thumbnail()) : ?>
                                <div class="card-img-container">
                                    
                                        <?php the_post_thumbnail('medium', array('class' => 'card-img borderRadius')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="card-img-overlay">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <p class="card-text"><?php the_excerpt(); ?></p>
                                <p class="card-text">Last updated 3 mins ago</p>
								<a href="#" class="animated-button">
								<span class="text">Click me</span>
    <span class="icon">🚀</span>

  </a>
                            </div>
                        </div>
					</a>	
                    </article>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
        else :
            ?>
            <div class="col">
                <p><?php esc_html_e('No hay entradas disponibles.', 'textdomain'); ?></p>
            </div>
        <?php
        endif;
        ?>
    </div>
</div>


	
	

	
	
	
</main>









<?php get_footer();